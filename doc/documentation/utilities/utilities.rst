.. module:: gpaw.utilities


Utilities
=========

.. autoclass:: gpaw.utilities.partition.AtomPartition
    :members:

.. module:: gpaw.utilities.ibz2bz
.. autofunction:: gpaw.utilities.ibz2bz.ibz2bz

.. module:: gpaw.utilities.dipole
.. autofunction:: gpaw.utilities.dipole.dipole_matrix_elements
.. autofunction:: gpaw.utilities.dipole.dipole_matrix_elements_from_calc

.. module:: gpaw.utilities.ekin
.. autofunction:: gpaw.utilities.ekin.ekin
